#!/usr/bin/env python3

import speedtest
import json
from datetime import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import yaml
import sys

# load config
ASANA_KEY = None
ASANA_WORKSPACE = None
ASANA_PROJECT = None
LAST_UPDATE_FILE = None
LAST_UPDATE_FILE_BAK = None
LAST_UPDATE_FILE_VARIABLE = "datetime="


mqtt_topic = None
mqtt_hostname = None
mqtt_port = 1883
mqtt_auth = None
cfg = None

with open("config.yml", "r") as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.CLoader)

if cfg == None:
    print("Cannot open config file")
    sys.exit()

mqtt_hostname = cfg["mqtt"]["hostname"]
mqtt_port = cfg["mqtt"]["port"]
if len(cfg["mqtt"]["username"]) > 0:
    mqtt_auth = {
        "username": cfg["mqtt"]["username"],
        "password": cfg["mqtt"]["password"],
    }
mqtt_topic = cfg["topics"]["speed"]

mqtt_client_id = "speedtest_1"

now = datetime.now()

servers = [6719]
# If you want to test against a specific server
# servers = [1234]

threads = None
# If you want to use a single threaded test
# threads = 1

s = speedtest.Speedtest(secure=True)
s.get_servers(servers)
s.get_best_server()
s.download(threads=threads)
s.upload(threads=threads)
# s.results.share()

results_dict = s.results.dict()

downloadSpeed = results_dict["download"] / 1024 / 1024
uploadSpeed = results_dict["upload"] / 1024 / 1024
pingValue = int(results_dict["ping"])

json_results = {
    "download": round(downloadSpeed, 2),
    "upload": round(uploadSpeed, 2),
    "ping": pingValue,
    "date": now.strftime("%m/%d %H:%M:%S"),
}

# Publish a single message to a broker, then disconnect cleanly.
publish.single(
    mqtt_topic,
    payload=json.dumps(json_results),
    qos=0,
    retain=True,
    hostname=mqtt_hostname,
    port=mqtt_port,
    client_id=mqtt_client_id,
    keepalive=60,
    will=None,
    auth=mqtt_auth,
    tls=None,
    protocol=mqtt.MQTTv311,
    transport="tcp",
)

print("Results-----------------")
print(json.dumps(json_results))
